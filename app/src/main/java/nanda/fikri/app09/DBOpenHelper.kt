package nanda.fikri.app09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(contet : Context) : SQLiteOpenHelper(contet, DB_NAME, null, DB_VER) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tVideo = "create table video(id_video text not null, id_cover text not null, title text not null)"
        val insert = "insert into video values('0x7f0c0000','0x7f060057','Birds Of Prey')," +
                "('0x7f0c0001','0x7f060058','Black Widow')," +
                "('0x7f0c0002','0x7f060061','Disney Mulan')," +
                "('0x7f0c0003','0x7f060062','Free Guy')," +
                "('0x7f0c0004','0x7f060065','Itaewon Class')," +
                "('0x7f0c0005','0x7f060074','1917')"
        db?.execSQL(tVideo)
        db?.execSQL(insert)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object{
        val DB_NAME = "video"
        val DB_VER = 1
    }

}